/**
 * Import function triggers from their respective submodules:
 *
 * const {onCall} = require("firebase-functions/v2/https");
 * const {onDocumentWritten} = require("firebase-functions/v2/firestore");
 *
 * See a full list of supported triggers at https://firebase.google.com/docs/functions
 */

const { onRequest } = require("firebase-functions/v2/https");
const admin = require("firebase-admin");
const { logger } = require("firebase-functions/v1");
const { log } = require("firebase-functions/logger");

//CONFIGURATIONS
admin.initializeApp({
  apiKey: "AIzaSyBsdGUQUahBaxJMjqNl-p1Q6F02gugJMGs",
  authDomain: "bs-bif.firebaseapp.com",
  projectId: "bs-bif",
  storageBucket: "bs-bif.appspot.com",
  messagingSenderId: "389025844621",
  appId: "1:389025844621:web:0e69097f5c33e9ea9381c4",
});

const auth = admin.auth();
// const db = admin.firestore();

exports.readByUID = onRequest({ cors: true }, async (request, reponse) => {
  const { uid } = request.body.data;

  const userInfo = await auth.getUser(uid);

  reponse.send({ data: userInfo }).status(200).json();
});

exports.readAllUsers = onRequest({ cors: true }, async (request, response) => {
  try {
    const result = await auth.listUsers(50);

    response.status(200).send({
      data: result.users,
    });
  } catch (error) {
    console.log(error);
    response.status(500).send({ message: "Ha ocurrido un error", error });
  }
});

exports.createUser = onRequest({ cors: true }, async (request, response) => {
  const data = request.body.data; //Data is the main node that have request payload

  try {
    let userCreated = await auth.createUser(data);

    //Adding custom claims for roles selected
    await auth.setCustomUserClaims(userCreated.uid, {
      roles: data.roles,
    });

    response
      .status(201)
      .send({ message: "¡Registro exitoso de usuario!", data: userCreated })
      .json();
  } catch (error) {
    response.status(500).json(error.code);
  }
});

exports.updateUser = onRequest({ cors: true }, async (request, response) => {
  const { uid, displayName, email, phoneNumber, photoURL, roles } =
    request.body.data;

  try {
    const userUpdated = await auth.updateUser(uid, {
      displayName,
      email,
      phoneNumber,
      photoURL,
      roles,
    });

    //Updating custom claims
    await auth.setCustomUserClaims(userUpdated.uid, { roles: roles });

    response
      .status(200)
      .json({ message: "User updated successfully", data: userUpdated });
  } catch (error) {
    console.log(error);
    response.status(500).json(error);
  }
});

exports.deleteUser = onRequest({ cors: true }, async (request, response) => {
  const { uid } = request.body.data;
  try {
    await auth.deleteUser(uid);
    response.status(200).send({ message: `User deleted: ${uid}`, data: null });
  } catch (error) {
    logger.error(error);
    response.status(500).json(error);
  }
});
